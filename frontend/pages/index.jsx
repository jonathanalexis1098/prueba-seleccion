import 'isomorphic-fetch'
import List from './List'

export default class extends React.Component {
    static async getInitialProps() {
        let URL = `http://localhost:3001/characters/`
        let req = await fetch(URL)
        let { characters } = await req.json()
        return { characters }
    }
    render() {
        const { characters } = this.props
        return <List characters={characters} />
    }
}
