import React, { Component } from 'react'
import View from './View'
import Link from 'next/link'

class List extends Component {
    

    render() {
        const { characters } = this.props
        return <div>
            {characters.map((character) => (
                <div>
                    <Link href={`/view?id=${character._id}`} prefetch key={character.id}>
                        <a className="channel">
                            <h2>{character.name}</h2>
                        </a>
                    </Link>
                </div>
            ))}
        </div>
    }
}
export default List