export default class Result extends React.Component {
    static async getInitialProps() {
        
    }
    static async getInitialProps({ query }) {
        let idCharacter = query.id
        let URL = `http://localhost:3001/characters/${idCharacter}`
        let req = await fetch(URL)
        let { character } = await req.json()
        return { character }
    }
    render() {
        const { character } = this.props

        return <div>
            {character.name}
        </div>
    }
}