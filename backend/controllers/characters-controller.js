const CharacterSchema = require('../models/characterSchema')

class Character {
    saveCharacter(req, res) {
        var character = new CharacterSchema()
        var params = req.body

        //Obtengo las variables de params y se las asigno a character
        // character = {...character,...params} doesn't work
        Object.assign(character, params)

        character.save((err, characterStored) => {
            if (err) return res.status(500).send({ message: "request error" })

            if (!characterStored) return res.status(404).send({ message: "the character could not be saved" })

            return res.status(200).send({ character: characterStored })
        })
    }
    saveAllCharacters(characters) {
        var characterSchema = new CharacterSchema()

        characterSchema.collection.insertMany(characters, function(err, docs) {
            if (err) {
                return console.error(err);
            } else {
                console.log(`Multiple documents inserted to Collection ${docs}`);
                return {data:docs}
            }
        });
    }
    getCharactersById(req, res) {
        var {id} = req.params

        if (id == null) return res.status(404).send({ message: "req error" })

        CharacterSchema.findById(id, (err, character) => {
            if (err) return res.status(500).send({
                message: "Error returning data"
            })
            if (!character) return res.status(404).send({ message: "the character does not exist"})

            return res.status(200).send({
                character
            })
        })
    }
    getAllCharacters(req, res) {
        CharacterSchema.find({
        }).exec((err, characters) => {
            if (err) return res.status(500).send({ message: "Error returning data" })
            if (!characters) return res.status(404).send({ message: "the characters does not exist" })

            return res.status(200).send({ characters })

        })
    }
}
module.exports = Character;