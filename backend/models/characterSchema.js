const { Schema, model } = require('mongoose')

const CharacterSchema = Schema({
    titles: [
        String
    ],
    books: [
        String
    ],
    _id: String,
    male: Boolean,
    house: String,
    slug: String,
    name: String,
    createdAt: String,
    updatedAt: String
},{ _id: false })


const Character = model('Character', CharacterSchema);
module.exports = Character