const express = require('express')
const CharacterController = require('../controllers/characters-controller')


const controller = new CharacterController()
var router = express.Router()


router.get('/characters',controller.getAllCharacters)
router.get('/characters/:id?',controller.getCharactersById)


module.exports = router