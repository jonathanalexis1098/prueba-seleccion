const mongoose = require('mongoose')
require('isomorphic-fetch');
const CharacterController = require('./controllers/characters-controller')
const controller = new CharacterController()
const url = 'mongodb://localhost:27017/got-api'
const option = { useNewUrlParser: true, useFindAndModify: false }
const app = require('./app')
const port = 3001

const conectarBD = (url, option) => {
    const success = () => {
        console.log("Congratulations! The connection to the database has been successful")

        app.listen(port, () => {
            console.log(`Server is running on port ${port}`)
        })
    }
    const error = err => console.log(err)
    mongoose.connect(url, option)
        .then(success)
        .catch(error)
}
conectarBD(url, option)

var apiRequest = async () => {
    let URL = `https://api.got.show/api/map/characters`
    let reqCharacters = await fetch(URL)
    reqCharacters = await reqCharacters.json()
    reqCharacters = reqCharacters.data

    return reqCharacters
}


const saveCharacters = async () => {
    let characters = await apiRequest()
    let URL = "http://localhost:3001/character"
    controller.saveAllCharacters(characters)    
}
saveCharacters()